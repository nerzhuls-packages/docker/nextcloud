FROM alpine:3.21.3

ENV NEXTCLOUD_MAIL_VERSION=v3.7.20
ENV NEXTCLOUD_CONTACTS_VERSION=v6.1.3
ENV NEXTCLOUD_CALENDAR_VERSION=v4.7.16
ENV PHONETRACK_VERSION=v0.7.4
ENV NEXTCLOUD_TASKS_VERSION=v0.14.5
ENV SNAPPYMAIL_VERSION=v2.38.2
ENV NEWS_VERSION=25.2.1

RUN apk add --no-cache curl ca-certificates && \
	cd /tmp && \
	curl -L -o mail.tar.gz https://github.com/nextcloud-releases/mail/releases/download/${NEXTCLOUD_MAIL_VERSION}/mail-${NEXTCLOUD_MAIL_VERSION}.tar.gz && \
	curl -L -o contacts.tar.gz  https://github.com/nextcloud-releases/contacts/releases/download/${NEXTCLOUD_CONTACTS_VERSION}/contacts-${NEXTCLOUD_CONTACTS_VERSION}.tar.gz && \
	curl -L -o calendar.tar.gz https://github.com/nextcloud-releases/calendar/releases/download/${NEXTCLOUD_CALENDAR_VERSION}/calendar-${NEXTCLOUD_CALENDAR_VERSION}.tar.gz && \
	curl -L -O https://github.com/nextcloud/tasks/releases/download/${NEXTCLOUD_TASKS_VERSION}/tasks.tar.gz && \
	curl -L -O https://github.com/nextcloud/news/releases/download/${NEWS_VERSION}/news.tar.gz && \
	curl -L -o snappymail.tar.gz https://snappymail.eu/repository/nextcloud/snappymail-${SNAPPYMAIL_VERSION:1}-nextcloud.tar.gz

RUN cd /tmp && \
	tar xvzf /tmp/mail.tar.gz && \
	tar xvzf /tmp/contacts.tar.gz && \
	tar xvzf /tmp/calendar.tar.gz && \
	tar xvzf /tmp/tasks.tar.gz && \
	tar xvzf /tmp/news.tar.gz && \
	tar xvzf /tmp/snappymail.tar.gz && \
	echo "Listen 8080" > /tmp/ports.conf

FROM nextcloud:30.0.6-apache

COPY --from=0 /tmp/contacts /usr/src/nextcloud/apps/contacts
COPY --from=0 /tmp/calendar /usr/src/nextcloud/apps/calendar
COPY --from=0 /tmp/tasks /usr/src/nextcloud/apps/tasks
COPY --from=0 /tmp/news /usr/src/nextcloud/apps/news
COPY --from=0 /tmp/snappymail /usr/src/nextcloud/apps/snappymail

COPY --from=0 /tmp/ports.conf /etc/apache2/ports.conf

EXPOSE 8080

RUN sed -i "/^.*# Update htaccess after init/i cp /var/www/config/config.php /var/www/html/config/config.php" /entrypoint.sh && \
	sed -i '/^.*# Update htaccess after init/i touch /var/www/html/data/.ocdata' /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]
