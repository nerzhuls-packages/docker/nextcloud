group "default" {
    targets = ["nextcloud"]
}

variable "VERSION" {
    default = "latest"
}

variable "PLATFORMS" {
    default = ["linux/amd64"]
}

target "nextcloud" {
    dockerfile = "./Dockerfile"
    platforms = PLATFORMS
    tags = ["registry.gitlab.com/nerzhuls-packages/docker/nextcloud:${VERSION}"]
}
